--------------------------------------------------------
--  DDL for Package Body SKILLS_API
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY "DEALTRACKERAPP"."SKILLS_API" as

  procedure add_skill(skill_name varchar2, skill_tnt_mapping varchar2) as
  
    var_skill_id number;
    var_current_user VARCHAR2(30);

  begin
        SELECT SKILL_ID_SEQ.nextval into var_skill_id from dual;
        select user into var_current_user from dual;
        
        INSERT INTO "DEALTRACKERAPP"."SKILLS" (
        skill_id,
        skill_desc,
        tnt_skill_mapping,
        created_by,
        created_date,
        last_modified_by,
        last_modified_date
    ) VALUES (
        var_skill_id,
        skill_name,
        skill_tnt_mapping,
        var_current_user,
        sysdate,
        var_current_user,
        sysdate
    );
  end add_skill;

end skills_api;

/
