--------------------------------------------------------
--  DDL for Package SKILLS_API
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE "DEALTRACKERAPP"."SKILLS_API" as 

  procedure add_skill(skill_name varchar2, skill_tnt_mapping varchar2);

end skills_api;

/
