alter session set "_ORACLE_SCRIPT"=true;

CREATE USER DealTrackerApp IDENTIFIED BY DealTrackerApp;

GRANT create session TO DealTrackerApp;
GRANT create table TO DealTrackerApp;
GRANT create view TO DealTrackerApp;
GRANT create any trigger TO DealTrackerApp;
GRANT create any procedure TO DealTrackerApp;
GRANT create sequence TO DealTrackerApp;
GRANT create synonym TO DealTrackerApp;
GRANT UNLIMITED TABLESPACE TO DealTrackerApp;

